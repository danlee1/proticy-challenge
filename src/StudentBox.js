// StudentBox.js
import React, { Component } from 'react';
import axios from 'axios';

import Student from './Student';
import style from './style';

class StudentBox extends Component{
    render(){
        return(
            <Student />
        )
    }
}

export default StudentBox;
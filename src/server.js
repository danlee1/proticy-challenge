// server.js 
'use strict'

// dependencies
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var Comment = require('../model/comments');
var Student = require('../model/student');


// instances
var app = express();
var router = express.Router();

// define port
var port = process.env.API_PORT || 3001;

// configure mongodb with Dan's mlab credentials (sandbox plan: free up to 0.5GB)
var mongoDB = 'mongodb://dan:2310##danEX@ds147265.mlab.com:47265/proticity-challenge';
mongoose.connect(mongoDB, {
  useMongoClient: true
})
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// configure api
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// prevent errors from Cross Origin Resource Sharing while disabling cacheing to ensure app renders most recent posts
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
  res.setHeader('Cache-Control', 'no-cache');
  next();
});

// set the route path and initialize the api
router.get('/', function (req, res) {
  res.json({
    message: 'API Initialized!'
  });
});

//Route for /students to our /api router
router.route('/students')
  .get(function (req, res) {
    Student.find(function (err, students) {
      if (err)
        res.send(err);
      res.json(students)
    });
  })
  .post(function (req, res) {
    console.log(req.body);
    var student = new Student();
    student.first_name = req.body.first_name;
    student.last_name = req.body.last_name;
    student.user_id =   req.body.user_id;

    student.save(function (err) {
      if (err)
        res.send(err);
      res.json({
        message: 'Student successfully created!'
      });
    });
  });
// adding the /comments route to our /api router
router.route('/comments')
  .get(function (req, res) {
    Comment.find(function (err, comments) {
      if (err)
        res.send(err);
      res.json(comments)
    });
  })
  // post new comment to the database
  // the CREATE method in CRUD
  .post(function (req, res) {
    var comment = new Comment();
    //body parser lets us use the req.body
    comment.author = req.body.author;
    comment.text = req.body.text;
    comment.save(function (err) {
      if (err)
        res.send(err);
      res.json({
        message: 'Comment successfully added!'
      });
    });
  });

//Create a route to access a specific student by id
router.route('/students/:student_id')
  .delete(function (req, res){
    Student.remove({
      _id: req.params.student_id
    }, function(err, comment){
      if(err)
        res.send(err);
      res.json({
        message: 'Student has been deleted.'
      })
    })
  });

// Adding a route to a specific comment 
router.route('/comments/:comment_id')
  // The put method allows for updating of comments based on the ID passed to the route
  // this is the other CREATE method in CRUD
  .put(function (req, res) {
    Comment.findById(req.params.comment_id, function (err, comment) {
      if (err)
        res.send(err);
      // set the new author and text to the new changes; if nothing was changed, do not alter the field.
      (req.body.author) ? comment.author = req.body.author: null;
      (req.body.text) ? comment.text = req.body.text: null;
      // save comment
      comment.save(function (err) {
        if (err)
          res.send(err);
        res.json({
          message: 'Comment has been updated'
        });
      });
    });
  })
  // finally, delete method for removing a comment from our database
  // and obviously is the DELETE method in CRUD 
  .delete(function (req, res) {
    //selects the comment by its ID, then removes it.
    Comment.remove({
      _id: req.params.comment_id
    }, function (err, comment) {
      if (err)
        res.send(err);
      res.json({
        message: 'Comment has been deleted'
      })
    })
  });

//Use our router configuration when we call /api
app.use('/api', router);

//starts the server and listens for requests
app.listen(port, function () {
  console.log(`api running on port ${port}`);
});
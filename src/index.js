// index.js
import React from 'react';
import ReactDOM from 'react-dom';
// import WebFont from 'webfontloader'; // https://scotch.io/@micwanyoike/how-to-add-fonts-to-a-react-project
import StudentBox from './StudentBox';
import CommentBox from './CommentBox';
import Header from './Header';
// import style from './style';

// WebFont.load({
//   google: { families: ['Poppins', 'sans-serif'] }
// });

ReactDOM.render(
  <div>
    <Header />
    <CommentBox
      url="http://dan-proticy-challenge.bitballoon.com/" 

      // when hosting with BitBalloon or any other hosting platform, must change the root before /api/comments
      
      pollInterval={2000} 
    />

    <hr />
    <StudentBox />


  </div>,
  document.getElementById("root")
);

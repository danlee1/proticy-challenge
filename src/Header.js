// Header.js
import React, { Component } from 'react';

import style from './style';

class Header extends Component {
  render() {
    return (
      <div>
        <h1 style={ style.title }>Dan's Proticy Challenge</h1>
        <h4 style={ style.title }>the mission: to create a CRUD-compliant web application with Express.</h4>
        <br />
        <br />
      </div>
    )
  }
}

export default Header;
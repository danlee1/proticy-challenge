# Dan's Proticy Challenge

### https://bitbucket.org/danlee1/proticy-challenge/

#
#

**Updated Objective**: Pull down this project, and make your own branch. Then create a new schema for students. I created the file for you already. The comments schema is there from before and can be used as reference, as well as all the other code in the server. Required fields are: first_name, last_name, and user_id. Then create a route for a second view that allows for just adding and then rendering the student schema on that second view. Do not worry the client side and deleting.

**Objective**: To build a web application that has CRUD functionality, with Express being the only required framework. Decided to use React for the Client-side framework. The app is a simple message board. The mLab platform is used for MongoDB. Source code in BitBucket repo.

### Getting Started
##### for developers
To start React client: 1. `npm start` 2. `http://localhost:3000`
**when you are ready to push up your new dev changes to src code, do not forget to rebuild the project: `npm run build`**
To start Express server: 1. `node ./src/server.js` 2. `http://localhost:3001/api`
Source code can be examined in my BitBucket repository: https://bitbucket.org/danlee1/proticy-challenge/
If you really need to checkout the database in mLab, just email me and we can figure something out.
##### for all users
First start Express server: 1. `node ./src/server.js` 2. `http://localhost:3001/api`
Then open ... and boom.

### Deployment
[BitBalloon](https://www.bitballoon.com/)

### Built With 
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
* [MongoDB (mLab)](https://mlab.com/) 
* [Axios](https://www.npmjs.com/package/axios)
* [Express](https://expressjs.com/)
* [Postman](https://www.getpostman.com)

### Authors
**[Dan Lee](https://www.linkedin.com/in/danlee4/)**

### License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Acknowledgments
the internet.
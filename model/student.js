//Import dependencies
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Create schema for our student model
var studentSchema = new Schema({
    first_name: { type: String, required: true },
    last_name:  { type: String, required: true },
    user_id:    { type: Number, required: true }
});

//Export our module to use in server.js
module.exports = mongoose.model('Student', studentSchema);